package de.ubt.ai4.petter.recpro.lib.filter.filterattributepersistence.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter

@Entity
@DiscriminatorValue("META")
public class FilterMetaAttributeInstance extends FilterAttributeInstance {
}
