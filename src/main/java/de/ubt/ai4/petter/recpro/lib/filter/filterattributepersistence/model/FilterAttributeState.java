package de.ubt.ai4.petter.recpro.lib.filter.filterattributepersistence.model;

public enum FilterAttributeState {
    TRUE,
    FALSE,
    NEUTRAL
}
