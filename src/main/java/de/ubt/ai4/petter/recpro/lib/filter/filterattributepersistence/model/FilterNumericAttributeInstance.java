package de.ubt.ai4.petter.recpro.lib.filter.filterattributepersistence.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
@DiscriminatorValue("NUMERIC")
public class FilterNumericAttributeInstance extends FilterAttributeInstance {
    private double value;
    private double minValue;
    private double maxValue;
    private boolean valueExact;
    private boolean valueBiggerThan;
    private boolean valueSmallerThan;
    private boolean valueBetween;
}
